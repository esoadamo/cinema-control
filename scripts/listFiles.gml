/*
Lists files from string
argument0 - files in format fFileName;dDirectoryName
*/
globalvar fileListObjects;
var i;
for(i = 0; i < ds_list_size(fileListObjects); i++){
    with(fileListObjects[| i]){
        instance_destroy();
    }
}
ds_list_clear(fileListObjects);

var input = argument0;
var array = string_split(input, ";");
var y_offset = 20;
var y_height = -1;
for(i = 0; i < array_length_1d(array); i++){
    var filename = array[i];
    if(string_length(filename) == 0){
        continue;
    }
    var targetSprite = sprFile;
    if(string_startsWith(filename, "d")){
        targetSprite = sprDir;
    }
    filename = string_delete(filename, 1, 1);
    var inst = instance_create(20, y_offset, objFile);
    fileListObjects[| i] = inst;
    inst.filename = filename;
    inst.sprite_index = targetSprite;
    if(y_height == -1){
        y_height = inst.sprite_height + 10;
    }
    y_offset += y_height;
}
