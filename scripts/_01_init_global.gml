///Run this on startup
globalvar settingsIni, connected, loadingText, directory, fileListObjects, directorySplitChar, newLineLength;
connected = false;
settingsIni = "settings.ini"
loadingText = "";
directorySplitChar = "/";
directory = directorySplitChar;
fileListObjects = ds_list_create();
newLineLength = 2;
