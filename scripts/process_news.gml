//argument0 - array of new commands
var i, http_resp;
for(i = 0; i < array_length_1d(argument0); i++){
http_resp = argument0[i];
if (string_startsWith(http_resp, "pD")){
    iid = string_split(http_resp, ";");
    var playerId, i, instanceId, iidi, tX, tY;
    for(i = 0; i < array_length_1d(iid); i ++){
        newInstanceCreated = false;
        iidi = string_split(iid[i],"=");
        if(iidi[0] == "p"){
            playerId = real(iidi[1]);
            instanceId = ds_list_find_value(global.listPlayers, playerId);
            if(instanceId == undefined || instanceId == -1  || !instance_exists(instanceId)){
                instanceId = instance_create(-500,-500,objPlayerRemote);
                global.listPlayers[| playerId] = instanceId;
                newInstanceCreated = true;
                send_data("getPlayerData="+string(playerId)+"&name");
            } else newInstanceCreated = false;
        } else if(iidi[0] == "x"){
            instanceId.x = real(iidi[1]);
        } else if(iidi[0] == "y"){
            tY = real(iidi[1]);
            if(not global.firstLoad and instanceId.y < 0)
                effect_lightning(instanceId.x, tY, true);
            instanceId.y = tY;
        } else if(iidi[0] == "s"){
            instanceId.speed = real(iidi[1]);
        } else if(iidi[0] == "d"){
            instanceId.direction = real(iidi[1]);
        } else  if(iidi[0] == "name"){
            instanceId.name = iidi[1];
        } else  if(iidi[0] == "loggedOut"){
            global.listPlayers[| playerId] = -1;
            tX = instanceId.x;
            tY = instanceId.y;
            if(instanceId != id and instance_exists(instanceId)) with(instanceId) instance_destroy(); 
            if(!newInstanceCreated and tX >= 0)
                effect_lightning(tX, tY, true);
        }
    }
    global.firstLoad = false;
} else if (string_startsWith(http_resp, "lighting")){
    iid = string_split(http_resp, ";");
    var i, iidi, tX;
    for(i = 0; i < array_length_1d(iid); i ++){
        iidi = string_split(iid[i],"=");
        if(iidi[0] == "x")
            tX = real(iidi[1]);
        else if(iidi[0] == "y")
            effect_lightning(tX, real(iidi[1]), true);
    }
    global.firstLoad = false;
} else if (http_resp == "more"){
    send_data("getNews");
}
}
