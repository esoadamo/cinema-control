///Counts player speed
/*
    If arugment count is not 0, takes argument 0 as instance id
*/
var instaceId;
if(argument_count > 0)
    instaceId = argument[0];
else
    instaceId = id;

return instaceId.moveSpeed * instaceId.speed_modifier;

