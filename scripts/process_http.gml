///Process entered data
globalvar newLineLength;
var http_resp, splitedString, iid, http_status, targetRoom;

http_resp = ds_map_find_value(async_load, "result");
http_status = ds_map_find_value(async_load, "status");
if(http_status < 0 && instance_exists(id)){ //That means error
    return false;
    //FIXME: This does not work when moved to another room
    show_debug_message("HTTP Error status: " + string(http_status));
    show_debug_message("HTTP Error raw status: " + string(ds_map_find_value(async_load, "http_status")));
    if(show_question("Error with communicating with server.#Do you want to return back to the main menu?")){
        room_goto(roomStartMenu);
        return false;
    }
}
if(is_undefined(http_resp))
    return false;

//var http_resp;
http_resp = string_delete(http_resp, string_length(http_resp) - newLineLength + 1, newLineLength);

if(string_length(http_resp) < 2)
    return false;

show_debug_message("HTTPin: '" + http_resp+"'")

if (string_startsWith(http_resp, "registere")){
    if(http_resp != "registered")
        newLineLength = 1;
    global.connected = true;
    room_goto(roomMainMenu);
}else if (http_resp == "quit"){
    send_data("bye");
    show_message("Server is shutting down");
    room_goto(roomStartMenu);
} else if(string_startsWith(http_resp, "d..")){
    listFiles(http_resp);
} else if(string_startsWith(http_resp, "playing")){
    if(room == roomMainMenu){
        objFile.filename = "Playing: " + string_delete(http_resp, 1, 7);
    }
}

