/*
argument0 - x
argument1 - y
argument2 - parent?
*/
var sEm;
if(argument2){
    with(instance_create(argument0, argument1, objLighting)){
        sEm = audio_emitter_create();
        audio_emitter_position(sEm, x, y, 0);
        audio_play_sound_on(sEm, sound_effect_lightning, false, 5);
        alarm[0] = 3.5 * room_speed;
        image_angle = choose(-15, 15);
        drawElipse = true;
        send_data("aUp=lighting;x="+string(x)+";y="+string(y)); 
    }
} else {
    with(instance_create(argument0, argument1, objLighting)) alarm[0] = 3 * room_speed; //Sound has 3.5 seconds
    image_angle = choose(-15, 15);
}

